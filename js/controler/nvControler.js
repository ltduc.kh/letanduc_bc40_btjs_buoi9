function layThongTinTuForm(){
     //lấy thông tin từ form
     const _tk = document.getElementById("tknv").value;
     const _name = document.getElementById("name").value;
     const _email = document.getElementById("email").value;
     const _password = document.getElementById("password").value;
     const _ngaylam = document.getElementById("datepicker").value ;
     const _luongCB = document.getElementById("luongCB").value * 1;
     const _chucvu = document.getElementById("chucvu").value;
     const _gioLam = document.getElementById("gioLam").value * 1;
    
     
     // tạo object nhân Viên
     var nv = new NhanVien(
          _tk,
          _name,
          _email,
          _password,
          _ngaylam,
          _luongCB,
          _chucvu,
          _gioLam 
     );
     return nv;
}

function renderDSNV(nvArr){
    var contentHTML = "";
    for (var index = 0; index < nvArr.length; index++){
        var item = nvArr[index];
        var contentTr = `<tr> 
                            <td>${item.tknv}</td>
                            <td>${item.name}</td>
                            <td>${item.email}</td>
                            <td>${item.datepicker}</td>
                            <td>${item.chucvu}</td>
                            <td>${item.tinhTongLuong()}</td>
                            <td>${item.xepLoai()}</td>
                            <td>
                            <button onclick="xoaNhanVien('${item.tknv}')" class="btn btn-danger" >Xoá</button>
                            </td>
                            
        
                        </tr>`;
     contentHTML += contentTr;
    }
document.getElementById("tableDanhSach").innerHTML = contentHTML;

}
function timKiemViTri(id, arr){
    var viTri = -1;
    for(var index = 0; index < arr.length; index++){
        var nv = arr[index];
        if (nv.tknv == id){
            viTri = index;
            break;
        }
    }
    return viTri;
}

