function kiemTraTrung(idNv, nvArr){
    var index = nvArr.findIndex(function(item){
        return idNv == item.tknv;
    }
    );
    if (index == -1){
        document.getElementById("tbTKNV").innerText = "";
        return true;
    }else {
        document.getElementById("tbTKNV").innerText = "Mã nv tồn tại"
        return false;
    }

    
}

function kiemTraDoDai(value, idErr, min, max){
    var length = value.length;
    if (length < min  || length > max){
        document.getElementById(idErr).innerText = `độ dài phải từ ${min} đến ${max} kí tự`
        return false;
    }else {
        document.getElementById(idErr).innerText = "";
        return true;
    }
}
function kiemTraSo(value, idErr){
    var reg = /^\d+$/;
    var isNumber = reg.test(value);
    if(isNumber){
        document.getElementById(idErr).innerText = "";
        return true;
    }else {
        document.getElementById(idErr).innerText= "trường nay"
        return false;
    }
}